INTRODUCTION
------------
The Webform CC module adds an optional cc header to emails sent by Webform.
Multiple addresses can be entered for a single email to address. Webform
components and tokens are supported as well. The default value can be set
globally with `variable_set('webform_cc_default_cc', 'example@example.com');`.

REQUIREMENTS
------------
Webform CC depends on Webform (https://www.drupal.org/project/webform).

INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.
